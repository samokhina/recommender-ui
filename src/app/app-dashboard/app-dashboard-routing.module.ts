import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AppDashboardComponent } from './app-dashboard.component';

const router: Routes = [
  {
    path: '',
    component: AppDashboardComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(router)],
  exports: [RouterModule],
})
export class AppDashboardRoutingModule {}
