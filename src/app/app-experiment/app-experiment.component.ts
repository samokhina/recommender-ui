import { Component, OnDestroy, OnInit } from '@angular/core';
import { ExperimentService } from './shared/services/experiment.service';
import { Experiment } from './shared/models/experiment';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { getRandomColor } from '../shared/functions/random-color';
import { ErrorFactor } from './shared/models/error-factor';
import { ErrorsFactor } from './shared/models/errors-factor';
import { IChartDataSet } from '../shared/components/interfaces/chart-data-set';
import { IExperimentData } from './shared/interfaces/experiment-data';

@Component({
  selector: 'app-experiment',
  templateUrl: './app-experiment.component.html',
  styleUrls: ['./app-experiment.component.scss'],
})
export class AppExperimentComponent implements OnInit, OnDestroy {
  public loadingSgd$: Observable<boolean>;
  public loadingAls$: Observable<boolean>;
  public experimentSgd$: Observable<IExperimentData>;
  public experimentAls$: Observable<IExperimentData>;

  private readonly unsubscribe: Subject<void>;
  private loadingSgd: BehaviorSubject<boolean>;
  private loadingAls: BehaviorSubject<boolean>;

  constructor(private experimentService: ExperimentService) {
    this.unsubscribe = new Subject();
    this.loadingSgd = new BehaviorSubject(true);
    this.loadingAls = new BehaviorSubject(true);
  }

  ngOnInit() {
    this.loadingSgd$ = this.loadingSgd
      .asObservable()
      .pipe(takeUntil(this.unsubscribe));
    this.loadingAls$ = this.loadingAls
      .asObservable()
      .pipe(takeUntil(this.unsubscribe));

    this.experimentSgd$ = this.getExperimentResultsSgd();
    this.experimentAls$ = this.getExperimentResultsAls();
  }

  public getExperimentResultsSgd(): Observable<IExperimentData> {
    return this.experimentService.getExperimentResultsSgd().pipe(
      map((experiment: Experiment) => {
        this.loadingSgd.next(false);
        return this.convertToChartData(experiment);
      })
    );
  }

  public getExperimentResultsAls(): Observable<IExperimentData> {
    return this.experimentService.getExperimentResultsAls().pipe(
      map((experiment: Experiment) => {
        this.loadingAls.next(false);
        return this.convertToChartData(experiment);
      })
    );
  }

  private convertToChartData(experiment: Experiment): IExperimentData {
    const testDataSets: IChartDataSet[] = [];
    const trainDataSets: IChartDataSet[] = [];
    const iterations: number[] = [];
    let iterationCurrent = 0;
    experiment.factors.forEach((errorsFactor: ErrorsFactor) => {
      const errorsTest: number[] = [];
      const errorsTrain: number[] = [];
      errorsFactor.errors.forEach((errorFactor: ErrorFactor) => {
        if (errorFactor.iteration > iterationCurrent) {
          iterationCurrent = errorFactor.iteration;
          iterations.push(iterationCurrent);
        }
        errorsTest.push(errorFactor.errorTest);
        errorsTrain.push(errorFactor.errorTrain);
      });
      const color = getRandomColor();
      const testDataSet: IChartDataSet = {
        label: errorsFactor.factor + ' факторов',
        data: errorsTest,
        borderColor: color,
      };
      const trainDataSet: IChartDataSet = {
        label: errorsFactor.factor + ' факторов',
        data: errorsTrain,
        borderColor: color,
      };
      testDataSets.push(testDataSet);
      trainDataSets.push(trainDataSet);
    });

    const experimentData: IExperimentData = {
      info: {
        rate: experiment.rate,
        regularization: experiment.regularization,
        maxIterations: experiment.maxIterations,
        dividingSample: experiment.dividingSample.split(',')[0],
      },
      test: {
        labels: iterations,
        datasets: testDataSets,
      },
      train: {
        labels: iterations,
        datasets: trainDataSets,
      },
    };

    return experimentData;
  }

  public ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
