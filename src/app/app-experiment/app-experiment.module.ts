import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppExperimentRoutingModule } from './app-experiment.routing.module';
import { AppExperimentComponent } from './app-experiment.component';
import { ChartModule } from '../shared/components/chart/chart.module';
import { ExperimentService } from './shared/services/experiment.service';
import { CommonModule } from '@angular/common';
import { ExperimentInfoModule } from '../shared/components/experiment-info/experiment-info.module';

@NgModule({
  declarations: [AppExperimentComponent],
  exports: [AppExperimentComponent],
  imports: [
    CommonModule,
    AppExperimentRoutingModule,
    FlexLayoutModule,
    ChartModule,
    ExperimentInfoModule,
  ],
  providers: [ExperimentService],
})
export class AppExperimentModule {}
