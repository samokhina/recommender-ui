import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AppExperimentComponent } from './app-experiment.component';

const router: Routes = [
  {
    path: '',
    component: AppExperimentComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(router)],
  exports: [RouterModule],
})
export class AppExperimentRoutingModule {}
