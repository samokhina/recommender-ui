import { IExperimentInfo } from '../../../shared/components/experiment-info/interfaces/experiment-info';
import { IChartData } from '../../../shared/components/interfaces/chart-data';

export interface IExperimentData {
  info: IExperimentInfo;
  test: IChartData;
  train: IChartData;
}
