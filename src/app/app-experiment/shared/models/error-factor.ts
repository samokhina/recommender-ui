export class ErrorFactor {
  // Ошибка на тестовой выборке
  public errorTest: number;

  // Ошибка на обучающей выборке
  public errorTrain: number;

  // Итерация
  public iteration: number;
}
