import { ErrorFactor } from './error-factor';
import { Type } from 'class-transformer';

export class ErrorsFactor {
  // Количество факторов
  public factor: number;

  // Ошибки на выборках для текущего количества факторов
  @Type(() => ErrorFactor)
  public errors: ErrorFactor[];
}
