import { ErrorsFactor } from './errors-factor';
import { Type } from 'class-transformer';

export class Experiment {
  // Регуляризация
  public regularization: number;

  // Скорость
  public rate: number;

  // Максимальное количество итераций
  public maxIterations: number;

  // Разделение выборки
  public dividingSample: string;

  // Ошибки на заданных факторах
  @Type(() => ErrorsFactor)
  public factors: ErrorsFactor[];
}
