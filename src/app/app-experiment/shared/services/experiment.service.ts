import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';
import { plainToClass } from 'class-transformer';
import { BaseService } from '../../../shared/services/base.service';
import { Experiment } from '../models/experiment';

@Injectable()
export class ExperimentService extends BaseService {
  private url = '/api/experiment/';

  constructor(http: HttpClient) {
    super(http);
  }

  getExperimentResultsSgd(): Observable<Experiment> {
    return this.get(this.url + `sgd`).pipe(
      map((response) => plainToClass(Experiment, response.body)),
      delay(1000)
    );
  }

  getExperimentResultsAls(): Observable<Experiment> {
    return this.get(this.url + `als`).pipe(
      map((response) => plainToClass(Experiment, response.body))
    );
  }
}
