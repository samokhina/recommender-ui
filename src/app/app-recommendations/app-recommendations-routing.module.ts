import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AppRecommendationsComponent } from './app-recommendations.component';

const router: Routes = [
  {
    path: '',
    component: AppRecommendationsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(router)],
  exports: [RouterModule],
})
export class AppRecommendationsRoutingModule {}
