import { Component, OnInit } from '@angular/core';
import { RecommenderService } from './shared/services/recommender.service';
import { BehaviorSubject, concat, Observable, of, Subject } from 'rxjs';
import { map, switchMap, takeUntil } from 'rxjs/operators';
import { Book } from './shared/models/book';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-recommendations',
  templateUrl: './app-recommendations.component.html',
  styleUrls: ['./app-recommendations.component.scss'],
})
export class AppRecommendationsComponent implements OnInit {
  public userId: string;
  public count: string;

  public loadingSgd$: Observable<boolean>;
  public loadingAls$: Observable<boolean>;
  public reload$: Observable<{ id: string; count: string }>;

  public recommendationsSgd$: Observable<Book[]>;
  public recommendationsAls$: Observable<Book[]>;

  private readonly unsubscribe: Subject<void>;
  private loadingSgd: BehaviorSubject<boolean>;
  private loadingAls: BehaviorSubject<boolean>;
  private reload: BehaviorSubject<{ id: string; count: string }>;

  constructor(
    private route: ActivatedRoute,
    private recommenderService: RecommenderService
  ) {
    this.unsubscribe = new Subject();
    this.loadingSgd = new BehaviorSubject(false);
    this.loadingAls = new BehaviorSubject(false);
    this.reload = new BehaviorSubject(null);
  }

  ngOnInit() {
    this.loadingSgd$ = this.loadingSgd
      .asObservable()
      .pipe(takeUntil(this.unsubscribe));
    this.loadingAls$ = this.loadingAls
      .asObservable()
      .pipe(takeUntil(this.unsubscribe));

    this.reload$ = this.reload.asObservable().pipe(takeUntil(this.unsubscribe));

    this.recommendationsSgd$ = this.reload$.pipe(
      switchMap((params) => {
        return params
          ? concat(
              of(null),
              this.getRecommendationsByUserSgd(+params.id, +params.count)
            )
          : of(null);
      })
    );

    this.recommendationsAls$ = this.reload$.pipe(
      switchMap((params) => {
        return params
          ? concat(
              of(null),
              this.getRecommendationsByUserAls(+params.id, +params.count)
            )
          : of(null);
      })
    );

    this.getParams();
  }

  private getParams(): void {
    this.userId = this.route.snapshot.queryParams['id']
      ? this.route.snapshot.queryParams['id']
      : '';
    this.count = this.route.snapshot.queryParams['count']
      ? this.route.snapshot.queryParams['count']
      : '';

    if (this.userId && this.count) {
      this.loadingSgd.next(true);
      this.loadingAls.next(true);
      this.reload.next({ id: this.userId, count: this.count });
    }
  }

  public getRecommendations(id: any, count: any): void {
    this.loadingSgd.next(true);
    this.loadingAls.next(true);
    this.reload.next({ id: id, count: count });
  }

  public getRecommendationsByUserSgd(
    userId: number,
    count: number
  ): Observable<Book[]> {
    return this.recommenderService
      .getRecommendationsByUserSgd(userId, count)
      .pipe(
        map((books: Book[]) => {
          this.loadingSgd.next(false);
          return books;
        })
      );
  }

  public getRecommendationsByUserAls(
    userId: number,
    count: number
  ): Observable<Book[]> {
    return this.recommenderService
      .getRecommendationsByUserAls(userId, count)
      .pipe(
        map((books: Book[]) => {
          this.loadingAls.next(false);
          return books;
        })
      );
  }
}
