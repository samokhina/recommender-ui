import { NgModule } from '@angular/core';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material';
import { AppRecommendationsComponent } from './app-recommendations.component';
import { AppRecommendationsRoutingModule } from './app-recommendations-routing.module';
import { RecommenderService } from './shared/services/recommender.service';
import { BookCardModule } from '../shared/components/book-card/book-card.module';
import { CommonModule } from '@angular/common';
import { FlexModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [AppRecommendationsComponent],
  exports: [AppRecommendationsComponent],
  imports: [
    AppRecommendationsRoutingModule,
    BookCardModule,
    CommonModule,
    FlexModule,
    FormsModule,
    MatInputModule,
    MatButtonModule,
  ],
  providers: [RecommenderService],
})
export class AppRecommendationsModule {}
