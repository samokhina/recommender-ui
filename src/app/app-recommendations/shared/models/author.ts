export class Author {
  // Идентификатор книги на сайте GoodReads.com
  public id: number;

  // Имя автора
  public name: string;

  // Количество оценок
  public ratingsCount: number;

  // Средний рейтинг
  public averageRating: number;

  // Фото автора
  public imageUrl: string;

  // Адрес автора на сайте GoodReads.com
  public url: number;
}
