import { Type } from 'class-transformer';
import { Author } from './author';

export class Book {
  // Идентификатор книги на сайте GoodReads.com
  public id: number;

  // Название книги
  public title: string;

  // Авторы книги
  @Type(() => Author)
  public authors: Author[];

  // Средний рейтинг
  public averageRating: number;

  // Описание
  public description: string;

  // Обложка книги
  public imageUrl: string;

  // Адрес книги на сайте GoodReads.com
  public url: number;
}
