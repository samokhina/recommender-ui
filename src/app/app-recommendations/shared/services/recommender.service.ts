import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';
import { plainToClass } from 'class-transformer';
import { BaseService } from '../../../shared/services/base.service';
import { Book } from '../models/book';

@Injectable()
export class RecommenderService extends BaseService {
  private url = '/api/recommender/';

  constructor(http: HttpClient) {
    super(http);
  }

  getRecommendationsByUserSgd(
    userId: number,
    countRecommendations: number
  ): Observable<Book[]> {
    const params = {
      recommendations: countRecommendations,
    };
    return this.get(this.url + `sgd/user/` + userId, null, params).pipe(
      map((response) => plainToClass<any, any>(Book, response.body))
    );
  }

  getRecommendationsByUserAls(
    userId: number,
    countRecommendations: number
  ): Observable<Book[]> {
    const params = {
      recommendations: countRecommendations,
    };
    return this.get(this.url + `als/user/` + userId, null, params).pipe(
      map((response) => plainToClass<any, any>(Book, response.body))
    );
  }
}
