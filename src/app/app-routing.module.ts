import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

const router: Routes = [
  {
    path: '',
    loadChildren: './app-dashboard/app-dashboard.module#AppDashboardModule',
  },
  {
    path: 'experiment',
    loadChildren: './app-experiment/app-experiment.module#AppExperimentModule',
  },
  {
    path: 'recommendations',
    loadChildren:
      './app-recommendations/app-recommendations.module#AppRecommendationsModule',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(router)],
  providers: [],
  exports: [RouterModule],
})
export class AppRoutingModule {}
