import { Component, Input, OnInit } from '@angular/core';
import { Book } from '../../../app-recommendations/shared/models/book';

@Component({
  selector: 'app-book-card',
  templateUrl: './book-card.component.html',
  styleUrls: ['./book-card.component.scss'],
})
export class BookCardComponent {
  @Input() public book: Book;
}
