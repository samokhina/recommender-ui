import { NgModule } from '@angular/core';
import { BookCardComponent } from './book-card.component';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material';

@NgModule({
  declarations: [BookCardComponent],
  exports: [BookCardComponent],
  providers: [],
  imports: [CommonModule, MatIconModule],
})
export class BookCardModule {}
