import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import * as Chart from 'chart.js';
import { isNullOrUndefined } from 'util';
import { getRandomColor } from '../../functions/random-color';
import { IChartData } from '../interfaces/chart-data';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
})
export class ChartComponent implements OnInit, AfterViewInit {
  @Input() public id: string;
  @Input() public width: string;
  @Input() public height: string;

  @Input()
  public set setDataChart(dataChart: IChartData) {
    if (
      !isNullOrUndefined(dataChart) &&
      dataChart.datasets &&
      dataChart.datasets.length > 0
    ) {
      this.dataChart = dataChart;
      this.dataChart.datasets = this.dataChart.datasets.map((value) => {
        const dataSet = { ...value, fill: false };
        if (!value.borderColor) {
          dataSet.borderColor = getRandomColor();
        }
        return dataSet;
      });
    }
  }

  private dataChart: IChartData;
  private canvas: any;
  private ctx: any;

  constructor() {
    this.id = 'chart';
    this.width = '650';
    this.height = '370';
  }

  ngOnInit() {}

  ngAfterViewInit() {
    this.canvas = document.getElementById(this.id);
    this.ctx = this.canvas.getContext('2d');
    const myChart = new Chart(this.ctx, {
      type: 'line',
      data: {
        labels: this.dataChart.labels,
        datasets: this.dataChart.datasets,
      },
      options: {
        responsive: false,
        display: true,
      },
    });
  }
}
