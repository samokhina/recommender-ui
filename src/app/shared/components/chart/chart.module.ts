import { NgModule } from '@angular/core';
import { ChartComponent } from './chart.component';

@NgModule({
  declarations: [ChartComponent],
  exports: [ChartComponent],
  providers: [],
})
export class ChartModule {}
