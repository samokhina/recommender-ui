export const EXPERIMENT_INFO_VALUES: any = {
  regularization: 'Регуляризация',
  maxIterations: 'Максимальное количество итераций',
  rate: 'Скорость',
  dividingSample: 'Процент тестовой выборки',
};
