import { Component, Input, OnInit } from '@angular/core';
import { IExperimentInfo } from './interfaces/experiment-info';
import { EXPERIMENT_INFO_VALUES } from './EXPERIMENT_INFO_VALUES';

@Component({
  selector: 'app-experiment-info',
  templateUrl: './experiment-info.component.html',
  styleUrls: ['./experiment-info.component.scss'],
})
export class ExperimentInfoComponent implements OnInit {
  @Input() public title: string;
  @Input() public info: IExperimentInfo;

  public values = EXPERIMENT_INFO_VALUES;

  constructor() {}

  ngOnInit() {}
}
