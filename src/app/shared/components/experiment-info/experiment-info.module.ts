import { NgModule } from '@angular/core';
import { ExperimentInfoComponent } from './experiment-info.component';
import { CommonModule } from '@angular/common';
import { PropertiesModule } from '../../pipes/properties.module';
import { FlexModule } from '@angular/flex-layout';

@NgModule({
  declarations: [ExperimentInfoComponent],
  exports: [ExperimentInfoComponent],
  providers: [],
  imports: [CommonModule, FlexModule, PropertiesModule],
})
export class ExperimentInfoModule {}
