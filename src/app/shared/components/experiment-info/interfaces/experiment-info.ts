export interface IExperimentInfo {
  rate: number;
  regularization: number;
  maxIterations: number;
  dividingSample: string;
}
