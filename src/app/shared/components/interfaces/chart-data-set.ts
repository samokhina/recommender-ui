export interface IChartDataSet {
  label: string;
  data: number[];
  borderColor?: string;
}
