import { IChartDataSet } from './chart-data-set';

export interface IChartData {
  labels: number[];
  datasets: IChartDataSet[];
}
