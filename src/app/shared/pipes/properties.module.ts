import { NgModule } from '@angular/core';
import { PropertiesPipe } from './properties.pipe';

@NgModule({
  declarations: [PropertiesPipe],
  exports: [PropertiesPipe],
  providers: [],
})
export class PropertiesModule {}
